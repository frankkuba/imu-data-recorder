import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Device {
	
	private StringProperty name = new SimpleStringProperty();
	
	private IntegerProperty port = new SimpleIntegerProperty();	
	
	public Device(String name, int port) {
		this.name.set(name);
		this.port.set(port);
	}


	public final StringProperty nameProperty() {
		return this.name;
	}
	

	public final String getName() {
		return this.nameProperty().get();
	}
	

	public final void setName(final String name) {
		this.nameProperty().set(name);
	}
	

	public final IntegerProperty portProperty() {
		return this.port;
	}
	

	public final int getPort() {
		return this.portProperty().get();
	}
	

	public final void setPort(int port) {
		this.portProperty().set(port);
	}
	
	
	
	

}
