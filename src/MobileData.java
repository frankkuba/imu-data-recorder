import java.io.File;
import java.net.URL;
import java.nio.file.Paths;

import com.sun.javafx.fxml.builder.URLBuilder;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MobileData extends Application {
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		
		
		FXMLLoader loader = new FXMLLoader();
	
		Parent root = loader.load(Paths.get("res/window.fxml").toUri().toURL());
		
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
		
		
		stage.setOnCloseRequest(e -> {
			Platform.exit();
			});
		
		

	}

}
