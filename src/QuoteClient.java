import java.io.*;
import java.net.*;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class QuoteClient {

	private boolean isRunning = true;
	DatagramPacket mDatagramPacket = null;
	DatagramSocket mDatagramSocket = null;
	BufferedWriter mBufferwriter;

	private int port;
	private String IPAdress;
	private File file;

	public QuoteClient(int port, String iPAdress, File file) {
		super();
		this.port = port;
		IPAdress = iPAdress;
		this.file = file;
	}

	public void cancel() {

		isRunning = false;
		
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	

		try {
			mBufferwriter.flush();
			mBufferwriter.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mDatagramSocket.close();

	}

	public void start() {

		InetAddress client_adress = null;
		try {

			client_adress = InetAddress.getByName(IPAdress);
		} catch (UnknownHostException e) {
			System.out.println(IPAdress + e.getMessage());
			return;

		}
		try {
			mDatagramSocket = new DatagramSocket(port, client_adress);
			
			mDatagramSocket.setReuseAddress(true);
		} catch (SocketException e) {
			mDatagramSocket = null;
			System.out.println(port + e.getMessage());
			return;
		}

		byte[] buf = new byte[256];

		try {

			mDatagramPacket = new DatagramPacket(buf, buf.length, client_adress, port);
		} catch (Exception e) {
			mDatagramSocket.close();
			mDatagramSocket = null;
			System.out.println(e.getMessage());
			return;
		}

		String text;
		double zeroTime;

		try {

			mBufferwriter = new BufferedWriter(new FileWriter(file));
			mBufferwriter.write("time,sensor,x,y,z\n");
			mDatagramSocket.receive(mDatagramPacket);
			text = new String(mDatagramPacket.getData(), 0, mDatagramPacket.getLength());
			zeroTime = Double.parseDouble(text.split(",")[0]);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return;
		}

		int i = 0;

		while (isRunning) {

			try {
				mDatagramSocket.receive(mDatagramPacket);
				text = new String(mDatagramPacket.getData(), 0, mDatagramPacket.getLength());
				
				String[] data = text.split(",");
				double time = Double.parseDouble(data[0]) - zeroTime;
				int offset = 4;
				
				for (int j = 1; j < data.length; j+=offset) {
					mBufferwriter.write(String.format(Locale.US, "%.5f,", time));
					System.out.print(String.format(Locale.US, "%.5f,", time));
					for (int k = j; k < j+offset ; k++) {
						if (k != (j+offset-1)) {
							mBufferwriter.write(data[k].trim() + ",");
							System.out.print(data[k].trim() + ",");
						}
						else {
							mBufferwriter.write(data[k].trim() + "\n");
							System.out.println(data[k].trim());
						}
					}
				}

				/*int index = text.indexOf(',');

				double time = (Double.parseDouble(text.substring(0, index))) - zeroTime;
				
				String out = String.format(Locale.US, "%.5f%s\n", time, text.substring(index));
				System.out.print(out);
				mBufferwriter.write(out);*/

			} catch (Exception e) {
				if (i < 10) {
					System.out.println("error during receiving, continue with another");
					e.printStackTrace();
					continue;
				} else {
					System.out.println("error during receiving, cancelling");
					break;
				}
			}

		}

	}
}
