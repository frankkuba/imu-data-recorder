
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.IntegerStringConverter;

public class loaderController implements Initializable {

	List<Task<Void>> tasks;
	
	ExecutorService exe;

	@FXML
	TableView<Device> table;

	@FXML
	Button stopBTN;

	@FXML
	TextField IPField, nameField, portField, experimentNameField;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		TableColumn<Device, String> nameCol = new TableColumn<Device, String>("name");
		TableColumn<Device, Integer> portCol = new TableColumn<Device, Integer>("port");

		nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
		portCol.setCellValueFactory(new PropertyValueFactory<>("port"));

		nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
		portCol.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));

		table.getColumns().addAll(nameCol, portCol);
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		
	}

	public void addDevice() {

		try {
			if (nameField.getText().isEmpty() && portField.getText().isEmpty())
				throw new Exception("Arguments not set");
			int port = Integer.parseInt(portField.getText());
			Device dev = new Device(nameField.getText(), port);
			table.getItems().add(dev);
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("Can not add device " + e.getMessage());
			alert.showAndWait();
		}

	}

	public void remove() {
		ObservableList<Device> list = table.getSelectionModel().getSelectedItems();
		table.getItems().removeAll(list);
	}

	public void start() {
		if (!IPField.getText().isEmpty() && !experimentNameField.getText().isEmpty()) {
			ObservableList<Device> devices = table.getItems();

			File dir = new File("./data/" + experimentNameField.getText());
			dir.mkdirs();
			List<QuoteClient> clients = new ArrayList<>();
			devices.forEach(cons -> {
				File file = new File(dir, cons.getName() + ".csv");
				clients.add(new QuoteClient(cons.getPort(), IPField.getText(), file));
			});
			
			
			tasks = new ArrayList<>(devices.size());
			clients.forEach(e -> tasks.add(getTask(e)));
			exe = Executors.newFixedThreadPool(10);
			tasks.forEach(e -> exe.execute(e));

			stopBTN.getParent().getChildrenUnmodifiable().forEach(cons -> cons.setDisable(true));
			stopBTN.setOnAction(e -> stop());
			stopBTN.setDisable(false);
		}
	}

	private Task<Void> getTask(QuoteClient client) {
		Task<Void> task = new Task<Void>() {
			QuoteClient taskClient;

			@Override
			public boolean cancel(boolean mayInterruptIfRunning) {
				taskClient.cancel();
				super.cancel(false);
				return super.isCancelled();

			}

			@Override
			protected Void call() throws Exception {
				taskClient = client;
				taskClient.start();
				return null;
			}
		};
		return task;
	}

	public void stop() {
		tasks.forEach(e -> e.cancel(false));
		System.out.println("cancelling");
		stopBTN.getParent().getChildrenUnmodifiable().forEach(cons -> cons.setDisable(false));
		stopBTN.setDisable(true);
		exe.shutdown();
		
	}
	
	
	

}
